#include "server/server.h"

#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>

namespace gp = google::protobuf;

namespace svr {

Server::Server(std::string server_address) : server_address_(std::move(server_address)) {

    grpc::ServerBuilder builder;
    builder.AddListeningPort(server_address_, grpc::InsecureServerCredentials());
    builder.RegisterService(this);

    server_ = builder.BuildAndStart();
    std::cout << "Server listening on " << server_address_ << std::endl;

    run_thread_ = std::thread([this] { server_->Wait(); });
}

Server::~Server() {
    // The deadline forces calls to terminate even if they aren't completed.
    // This is necessary because the client is using a continuous streaming call.
    auto deadline = std::chrono::system_clock::now() + std::chrono::milliseconds(100);
    server_->Shutdown(deadline);

    // Wait for the server to finish
    run_thread_.join();
}

grpc::Status
Server::echo(grpc::ServerContext* /*context*/, const proj::proto::EchoData* request, proj::proto::EchoData* response) {
    response->CopyFrom(*request);
    return grpc::Status::OK;
}

} // namespace svr
