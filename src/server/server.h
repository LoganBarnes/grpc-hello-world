#pragma once

#include <proj/server.grpc.pb.h>

#include <memory>
#include <thread>

namespace svr {

class Server : private proj::proto::EchoServer::Service {
public:
    explicit Server(std::string server_address);
    ~Server() override;

private:
    std::string server_address_;
    std::unique_ptr<grpc::Server> server_;

    std::thread run_thread_;

    grpc::Status
    echo(grpc::ServerContext* context, const proj::proto::EchoData* request, proj::proto::EchoData* response) override;
};

} // namespace svr
